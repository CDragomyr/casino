# Test application #

Test task listed at [file](ot-test-assignment.pdf)

## How to run ##

### To start Spring boot ###
`$ ./gradlew bootRun`

### Play endpoint with sample request ###

Sample of request
```
POST http://localhost:8080/api/v1/play
Accept: application/json
Content-Type:  application/json;charset=UTF-8

{
"bet": 1
}
```

Sample of response
```
{
  "win": 114,
  "balance": 114,
  "winningCombinations": {
    "top row": "Ace",
    "middle row": "Nine",
    "bottom row": "Eight"
  },
  "reels": {
    "leftReel": [
      "Ace",
      "Nine",
      "Eight"
    ],
    "middleReel": [
      "Ace",
      "Nine",
      "Eight"
    ],
    "rightReel": [
      "Ace",
      "Nine",
      "Eight"
    ]
  }
}
```

### Files ###
* configure reels - `src/main/resources/reels.csv`
* configure rates - `src/main/resources/rates.csv`
