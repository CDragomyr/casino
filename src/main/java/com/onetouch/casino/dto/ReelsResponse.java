package com.onetouch.casino.dto;

import java.util.List;

import lombok.Data;

@Data
public class ReelsResponse {

    private List<String> leftReel;
    private List<String> middleReel;
    private List<String> rightReel;
}
