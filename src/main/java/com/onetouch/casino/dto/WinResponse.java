package com.onetouch.casino.dto;

import java.math.BigDecimal;
import java.util.Map;

import lombok.Data;

@Data
public class WinResponse {

    private Integer win;
    private BigDecimal balance;
    private Map<String, String> winningCombinations;
    private ReelsResponse reels;
}
