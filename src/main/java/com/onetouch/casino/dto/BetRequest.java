package com.onetouch.casino.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class BetRequest {

    @NotNull
    @Min(value = 1, message = "must be in range between 1 and 10")
    @Max(value = 10, message = "must be in range between 1 and 10")
    private Integer bet;
}
