package com.onetouch.casino.convertor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.onetouch.casino.dto.ReelsResponse;
import com.onetouch.casino.dto.WinResponse;
import com.onetouch.casino.model.Reels;
import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.model.Win;
import com.onetouch.casino.model.WinningRow;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CasinoConvertor {

    public WinResponse toResponse(Win model) {
        var reels = convertReels(model.getReels());
        var winningCombinations = convertWinningCombinations(model.getWinningCombinations());

        var winResponse = new WinResponse();
        winResponse.setWin(model.getWin());
        winResponse.setBalance(model.getBalance());
        winResponse.setWinningCombinations(winningCombinations);
        winResponse.setReels(reels);

        return winResponse;
    }

    private Map<String, String> convertWinningCombinations(Map<WinningRow, Symbol> winningCombinations) {
        return winningCombinations.keySet().stream()
                                  .collect(
                                          Collectors.toMap(
                                                  WinningRow::getDescription,
                                                  o -> getSymbolName(winningCombinations.get(o)))
                                  );
    }


    private ReelsResponse convertReels(Reels reels) {
        var leftReelNames = getSymbolNames(reels.getLeft());
        var middleReelNames = getSymbolNames(reels.getMiddle());
        var rightReelNames = getSymbolNames(reels.getRight());

        var reelsResponse = new ReelsResponse();
        reelsResponse.setLeftReel(leftReelNames);
        reelsResponse.setMiddleReel(middleReelNames);
        reelsResponse.setRightReel(rightReelNames);

        return reelsResponse;
    }

    private String getSymbolName(Symbol symbol) {
        return StringUtils.capitalize(symbol.name().toLowerCase());
    }

    private List<String> getSymbolNames(List<Symbol> reel) {
        return reel.stream()
                   .map(Enum::name)
                   .map(String::toLowerCase)
                   .map(StringUtils::capitalize)
                   .collect(Collectors.toList());
    }
}
