package com.onetouch.casino.service;

import java.util.Map;

import com.onetouch.casino.model.Symbol;

public interface RateService {

    void loadRates();
    Integer getBaseWin(Symbol symbol);
    Map<Symbol, Integer> getRates();
}
