package com.onetouch.casino.service;

import java.math.BigDecimal;

public interface BalanceService {

    BigDecimal add(Integer payoff);
}
