package com.onetouch.casino.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.onetouch.casino.model.Reels;
import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.model.Win;
import com.onetouch.casino.model.WinningRow;
import com.onetouch.casino.service.BalanceService;
import com.onetouch.casino.service.PlayService;
import com.onetouch.casino.service.RateService;
import com.onetouch.casino.service.ReelService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PlayServiceImpl implements PlayService {

    private static final int NO_WIN = 0;

    private final RateService rateService;
    private final ReelService reelService;
    private final BalanceService balanceService;

    private static Map<WinningRow, Symbol> fetchFromDiagonals(Reels reels) {
        var superSymbols = new HashMap<WinningRow, Symbol>();
        fromLeftUpperCorner(reels).ifPresent(symbol -> superSymbols.put(WinningRow.DIAGONAL_FROM_LEFT_UPPER_CORNER, symbol));
        fromRightUpperCorner(reels).ifPresent(symbol -> superSymbols.put(WinningRow.DIAGONAL_FROM_RIGHT_UPPER_CORNER, symbol));

        return superSymbols;
    }

    private static Optional<Symbol> fromLeftUpperCorner(Reels reels) {
        if (reels.getLeft().get(0) == reels.getMiddle().get(1) && reels.getLeft().get(0) == reels.getRight().get(2)) {
            return Optional.of(reels.getLeft().get(0));
        }

        return Optional.empty();
    }

    private static Optional<Symbol> fromRightUpperCorner(Reels reels) {
        if (reels.getLeft().get(2) == reels.getMiddle().get(1) && reels.getLeft().get(2) == reels.getRight().get(0)) {
            return Optional.of(reels.getLeft().get(2));
        }

        return Optional.empty();
    }

    private static Map<WinningRow, Symbol> fetchFromRows(Reels reels) {
        var combinations = new HashMap<WinningRow, Symbol>();

        for (var i = 0; i < reels.getLeft().size(); i++) {
            var leftSymbol = reels.getLeft().get(i);
            var middleSymbol = reels.getMiddle().get(i);
            var rightSymbol = reels.getRight().get(i);
            if (leftSymbol == middleSymbol && leftSymbol == rightSymbol) {
                var winningRow = WinningRow.fromRowIndex(i);
                combinations.put(winningRow, leftSymbol);
            }
        }

        return combinations;
    }

    private static Map<WinningRow, Symbol> fetchWinningCombinations(Reels reels) {
        var combinations = fetchFromRows(reels);
        var diagonalCombinations = fetchFromDiagonals(reels);

        combinations.putAll(diagonalCombinations);

        return combinations;
    }

    @Override
    public Win play(Integer bet) {
        var selectedSymbols = reelService.spin();

        var combinations = fetchWinningCombinations(selectedSymbols);
        var win = calculateWin(bet, combinations.values());
        var totalBalance = balanceService.add(win);

        return new Win(win, totalBalance, combinations, selectedSymbols);
    }

    private Integer calculateWin(Integer bet, Collection<Symbol> symbols) {
        if (CollectionUtils.isEmpty(symbols)) {
            return NO_WIN;
        }

        return symbols.stream()
                      .map(rateService::getBaseWin)
                      .map(value -> value * bet)
                      .reduce(NO_WIN, Integer::sum);
    }
}
