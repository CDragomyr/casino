package com.onetouch.casino.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.service.RateService;
import com.onetouch.casino.utils.FileReaderUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RateServiceImpl implements RateService {

    private static final int RATE_DATA_COLUMN = 2;

    private Map<Symbol, Integer> rates;

    @Value("${pay.table.filename:rates.csv}")
    private String filename;

    @Override
    @PostConstruct
    public void loadRates() {
        var rates = new HashMap<Symbol, Integer>();
        var csvRecords = FileReaderUtils.readFile(filename);
        for (var csvRecord : csvRecords) {
            var values = csvRecord.toList();
            if (values.size() != RATE_DATA_COLUMN) {
                log.warn("Invalid data structure for rate on {} row in {} file. Row will bw skipped", csvRecord.getRecordNumber(), filename);
            }
            var symbol = Symbol.from(values.get(0));
            var rate = Integer.valueOf(values.get(1));
            if (rates.containsKey(symbol)) {
                log.warn("{} symbol is already present in rates and will be overridden with {} value", values.get(0), rate);
            }
            rates.put(symbol, rate);
        }

        this.rates = rates;
    }

    @Override
    public Integer getBaseWin(final Symbol symbol) {
        return rates.get(symbol);
    }

    @Override
    public Map<Symbol, Integer> getRates() {
        return rates;
    }
}
