package com.onetouch.casino.service.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.onetouch.casino.service.BalanceService;

@Service
public class BalanceServiceImpl implements BalanceService {

    private BigDecimal balance = BigDecimal.ZERO;

    @Override
    public BigDecimal add(Integer payoff) {
        var wonBigDecimal = BigDecimal.valueOf(payoff);
        balance = balance.add(wonBigDecimal);
        return balance;
    }
}
