package com.onetouch.casino.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.onetouch.casino.model.Reels;
import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.service.ReelService;
import com.onetouch.casino.utils.FileReaderUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ReelServiceImpl implements ReelService {

    private static final int AMOUNT_OF_REELS = 3;

    private Reels reels;

    @Value("${reels.filename:reels.csv}")
    private String filename;

    private static void addSymbolToReel(Map<Integer, List<Symbol>> reels, int reelIndex, Symbol symbol) {
        var symbols = reels.get(reelIndex);
        symbols.add(symbol);
    }

    private static void addSymbolToNewReel(Map<Integer, List<Symbol>> reels, int i, Symbol symbol) {
        var symbols = new ArrayList<Symbol>();
        symbols.add(symbol);
        reels.put(i, symbols);
    }

    private static void populateReels(Map<Integer, List<Symbol>> reels, List<String> values) {
        for (int reelIndex = 0; reelIndex < values.size(); reelIndex++) {
            var value = values.get(reelIndex);
            var symbol = Symbol.from(value);
            if (!reels.containsKey(reelIndex)) {
                addSymbolToNewReel(reels, reelIndex, symbol);
            } else {
                addSymbolToReel(reels, reelIndex, symbol);
            }
        }
    }

    private static List<Symbol> spinReel(List<Symbol> reel) {
        List<Symbol> symbols;
        var index = RandomUtils.nextInt(0, reel.size());
        if (index + AMOUNT_OF_REELS > reel.size()) {
            symbols = new ArrayList<>(reel.subList(index, reel.size()));
            var symbolsFromBeginning = index + AMOUNT_OF_REELS - reel.size();
            var part = reel.subList(0, symbolsFromBeginning);
            symbols.addAll(part);
        } else {
            symbols = reel.subList(index, index + AMOUNT_OF_REELS);
        }
        return symbols;
    }

    @Override
    @PostConstruct
    public void loadReels() {
        var reels = new HashMap<Integer, List<Symbol>>();
        var csvRecords = FileReaderUtils.readFile(filename);
        for (var csvRecord : csvRecords) {
            var values = csvRecord.toList();
            if (values.size() != AMOUNT_OF_REELS) {
                log.warn("Record {} (line - {}) from {} file is not corresponding to require the amount of reels (amount of reels-{}). Record will be skipped",
                         StringUtils.join(values), csvRecord.getRecordNumber(), filename, AMOUNT_OF_REELS);
                continue;
            }

            populateReels(reels, values);
        }

        this.reels = new Reels(reels.get(0), reels.get(1), reels.get(2));
    }

    @Override
    public Reels spin() {
        var leftReel = spinReel(reels.getLeft());
        var middleReel = spinReel(reels.getMiddle());
        var rightReel = spinReel(reels.getRight());

        return new Reels(leftReel, middleReel, rightReel);
    }

    @Override
    public Reels getReels() {
        return reels;
    }
}
