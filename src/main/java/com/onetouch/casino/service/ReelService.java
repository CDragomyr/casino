package com.onetouch.casino.service;

import com.onetouch.casino.model.Reels;

public interface ReelService {

    void loadReels();
    Reels spin();
    Reels getReels();
}
