package com.onetouch.casino.service;

import com.onetouch.casino.model.Win;

public interface PlayService {

    Win play(Integer bet);
}
