package com.onetouch.casino.controller;

import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.onetouch.casino.convertor.CasinoConvertor;
import com.onetouch.casino.dto.BetRequest;
import com.onetouch.casino.dto.WinResponse;
import com.onetouch.casino.service.PlayService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class CasinoController {

    private final PlayService playService;

    @PostMapping(value = "/play")
    public WinResponse play(@Valid @RequestBody BetRequest request) {
        var win = playService.play(request.getBet());
        return CasinoConvertor.toResponse(win);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException.class)
    public Map<String, String> exception(BindException ex) {
        return ex.getBindingResult().getAllErrors().stream()
                 .collect(Collectors.toMap(
                         error -> ((FieldError) error).getField(),
                         o -> StringUtils.defaultIfBlank(o.getDefaultMessage(), "")));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Map<String, String> exception(HttpMessageNotReadableException ex) {
        return Map.of("message", ex != null ? ex.getMessage() : "request cannot be parsed");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Throwable.class)
    public Map<String, String> exception(Throwable ex) {
        return Map.of("message", ex != null ? ex.getMessage() : "request cannot be parsed");
    }
}
