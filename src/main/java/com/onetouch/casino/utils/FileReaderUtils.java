package com.onetouch.casino.utils;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class FileReaderUtils {

    public List<CSVRecord> readFile(String filename) {
        if (StringUtils.isBlank(filename)) {
            log.warn("Filename was not provided. Actual value - [{}]", filename);
            return Collections.emptyList();
        }
        try (var inputStream = FileReaderUtils.class.getClassLoader().getResourceAsStream(filename)) {
            try (var fileReader = new InputStreamReader(inputStream)) {
                try (var csvRecords = new CSVParser(fileReader, CSVFormat.DEFAULT)) {
                    var records = csvRecords.getRecords();
                    log.info("Read {} lines from {} file", records.size(), filename);
                    return records;
                }
            }
        } catch (IOException e) {
            log.error("Cannot read {} file", filename, e);
        } catch (RuntimeException ex) {
            log.error("Unknown error occurred. File name - {}", filename, ex);
        }
        return Collections.emptyList();
    }
}
