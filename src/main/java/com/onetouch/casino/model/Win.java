package com.onetouch.casino.model;

import java.math.BigDecimal;
import java.util.Map;

import lombok.Value;

@Value
public class Win {

    Integer win;
    BigDecimal balance;
    Map<WinningRow, Symbol> winningCombinations;
//    List<Symbol> symbols;
    Reels reels;
}
