package com.onetouch.casino.model;

import java.util.Arrays;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum WinningRow {

    TOP_ROW(0, "top row"),
    MIDDLE_ROW(1, "middle row"),
    BOTTOM_ROW(2, "bottom row"),
    DIAGONAL_FROM_LEFT_UPPER_CORNER(-1, "diagonal from left upper corner"),
    DIAGONAL_FROM_RIGHT_UPPER_CORNER(-2, "diagonal from right upper corner");

    private final int rowIndex;
    private final String description;

    public static WinningRow fromRowIndex(final int rowIndex) {
        return Arrays.stream(values())
                .filter(winningRow -> winningRow.rowIndex == rowIndex)
                .findFirst()
                .orElseThrow();
    }
}
