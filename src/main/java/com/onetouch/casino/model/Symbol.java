package com.onetouch.casino.model;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

public enum Symbol {

    ACE,
    KING,
    QUEEN,
    NIGHT,
    TEN,
    NINE,
    EIGHT,
    SEVEN;

    public static Symbol from(String value) {
        return Arrays.stream(values())
                .filter(symbol -> StringUtils.equalsIgnoreCase(symbol.name(), value))
                .findFirst()
                .orElseThrow();
    }
}
