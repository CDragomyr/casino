package com.onetouch.casino.model;

import java.util.List;

import lombok.Value;

@Value
public class Reels {

    List<Symbol> left;
    List<Symbol> middle;
    List<Symbol> right;
}
