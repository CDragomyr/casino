package com.onetouch.casino.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;

class FileReaderUtilsTest {

    private ListAppender<ILoggingEvent> listAppender;

    private static Stream<Arguments> invalidFilenameProvider() {
        return Stream.of(
                Arguments.of((String) null),
                Arguments.of("")
        );
    }

    private static Stream<Arguments> validFilenameProvider() {
        return Stream.of(
                Arguments.of("rates.csv", 8),
                Arguments.of("reels.csv", 20)
        );
    }

    @BeforeEach
    void beforeEach() {
        final var logger = (Logger) LoggerFactory.getLogger(FileReaderUtils.class);
        listAppender = new ListAppender<>();
        listAppender.start();
        logger.addAppender(listAppender);
    }

    @AfterEach
    void after() {
        listAppender.stop();
    }

    @Test
    void readFile_incorrectFilename() {
        assertThat(FileReaderUtils.readFile("somefilename")).isEmpty();
        var logs = listAppender.list;
        var tuples = new Tuple[]{
                tuple(Level.ERROR, "Unknown error occurred. File name - somefilename")
        };
        assertThat(logs).size().isEqualTo(1).returnToIterable()
                        .extracting("level", "formattedMessage")
                        .containsOnly(tuples);
    }

    @ParameterizedTest
    @MethodSource("invalidFilenameProvider")
    void readFile_invalidFilename(String filename) {
        assertThat(FileReaderUtils.readFile("")).isEmpty();

        var logs = listAppender.list;
        var tuples = new Tuple[]{
                tuple(Level.WARN, "Filename was not provided. Actual value - [" + StringUtils.defaultIfBlank(filename, "") + "]")
        };
        assertThat(logs).size().isEqualTo(1).returnToIterable()
                        .extracting("level", "formattedMessage")
                        .containsOnly(tuples);

    }

    @ParameterizedTest
    @MethodSource("validFilenameProvider")
    void readFile(String filename, int lines) {
        assertThat(FileReaderUtils.readFile(filename)).isNotEmpty();

        var logs = listAppender.list;
        var tuples = new Tuple[]{
                tuple(Level.INFO, String.format("Read %s lines from %s file", lines, filename))
        };
        assertThat(logs).size().isEqualTo(1).returnToIterable()
                        .extracting("level", "formattedMessage")
                        .containsOnly(tuples);
    }
}