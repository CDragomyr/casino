package com.onetouch.casino.controller;

import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.onetouch.casino.model.Reels;
import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.model.Win;
import com.onetouch.casino.model.WinningRow;
import com.onetouch.casino.service.PlayService;

@WebMvcTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class CasinoControllerTest {

    @MockBean
    private PlayService playService;

    @Autowired
    private MockMvc mockMvc;

    private static Stream<Arguments> validBetProvider() {
        return Stream.of(
                Arguments.of(1),
                Arguments.of(5),
                Arguments.of(10)
        );
    }

    private static Stream<Arguments> invalidBetProvider() {
        return Stream.of(
                Arguments.of(-1),
                Arguments.of(0),
                Arguments.of(11)
        );
    }

    private static Stream<Arguments> nullProvider() {
        return Stream.of(
                Arguments.of("{}"),
                Arguments.of("{\"bet\":\"\"}")
        );
    }

    private static Stream<Arguments> syntaxErrorProvider() {
        return Stream.of(
                Arguments.of("[]"),
                Arguments.of("{bet}"),
                Arguments.of("{\"bet\":}"),
                Arguments.of("{\"bet\":abc}"),
                Arguments.of("{\"bet\":\"abc\"}"),
                Arguments.of("\"\""),
                Arguments.of("abc")
        );
    }

    private static Stream<Arguments> contentTypeProvider() {
        return Stream.of(
                Arguments.of(MediaType.APPLICATION_XML_VALUE),
                Arguments.of(MediaType.TEXT_PLAIN_VALUE),
                Arguments.of(MediaType.TEXT_XML_VALUE)
        );
    }

    private ResultActions performRequest(String request, String contentType) throws Exception {
        var req = MockMvcRequestBuilders.post("/api/v1/play")
                                        .content(request)
                                        .contentType(contentType);
        return mockMvc.perform(req);
    }

    @ParameterizedTest
    @MethodSource("validBetProvider")
    void play_validBet_noWin(Integer bet) throws Exception {
        var request = String.format("{\"bet\": %s}", bet);

        when(playService.play(anyInt())).thenReturn(new Win(0, BigDecimal.ZERO, Map.of(), new Reels(List.of(), List.of(), List.of())));

        performRequest(request, MediaType.APPLICATION_JSON_VALUE)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.win").value(0))
                .andExpect(jsonPath("$.balance").value(0))
                .andExpect(jsonPath("$.winningCombinations").isEmpty())
                .andExpect(jsonPath("$.reels").exists())
                .andExpect(jsonPath("$.reels.leftReel").isEmpty())
                .andExpect(jsonPath("$.reels.middleReel").isEmpty())
                .andExpect(jsonPath("$.reels.rightReel").isEmpty());
    }

    @ParameterizedTest
    @MethodSource("validBetProvider")
    void play_validBet_win(Integer bet) throws Exception {
        var request = String.format("{\"bet\": %s}", bet);

        var winningCombinations = Map.of(
                WinningRow.TOP_ROW, Symbol.EIGHT,
                WinningRow.MIDDLE_ROW, Symbol.NINE,
                WinningRow.BOTTOM_ROW, Symbol.ACE);
        var reels = new Reels(List.of(Symbol.EIGHT, Symbol.NINE, Symbol.ACE), List.of(Symbol.EIGHT, Symbol.NINE, Symbol.ACE), List.of(Symbol.EIGHT, Symbol.NINE, Symbol.ACE));
        when(playService.play(anyInt())).thenReturn(new Win(0, BigDecimal.TEN, winningCombinations, reels));

        performRequest(request, MediaType.APPLICATION_JSON_VALUE)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.win").value(0))
                .andExpect(jsonPath("$.balance").value(10))
                .andExpect(jsonPath("$.winningCombinations").isMap())
                .andExpect(jsonPath("$.reels").exists())
                .andExpect(jsonPath("$.reels.leftReel").isArray())
                .andExpect(jsonPath("$.reels.middleReel").isArray())
                .andExpect(jsonPath("$.reels.rightReel").isArray());
    }

    @ParameterizedTest
    @MethodSource("invalidBetProvider")
    public void play_invalidBet(Number invalidBet) throws Exception {
        var request = String.format("{\"bet\": %s}", invalidBet);

        performRequest(request, MediaType.APPLICATION_JSON_VALUE)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.bet").value("must be in range between 1 and 10"));
    }

    @ParameterizedTest
    @MethodSource("nullProvider")
    public void incorrectRequest(String request) throws Exception {
        performRequest(request, MediaType.APPLICATION_JSON_VALUE)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.bet").value("must not be null"));
    }

    @ParameterizedTest
    @MethodSource("syntaxErrorProvider")
    public void syntaxError(String request) throws Exception {
        performRequest(request, MediaType.APPLICATION_JSON_VALUE)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", startsWith("JSON parse error")));
    }

    @ParameterizedTest
    @MethodSource("contentTypeProvider")
    public void contentType(String contentType) throws Exception {
        var request = "{\"bet\": 1}";
        performRequest(request, contentType)
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message", Matchers.matchesPattern("Content type '.*?' not supported")));
    }
}