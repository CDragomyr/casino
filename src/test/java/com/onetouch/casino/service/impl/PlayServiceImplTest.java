package com.onetouch.casino.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.onetouch.casino.model.Reels;
import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.model.WinningRow;
import com.onetouch.casino.service.BalanceService;
import com.onetouch.casino.service.RateService;
import com.onetouch.casino.service.ReelService;

@ExtendWith(MockitoExtension.class)
class PlayServiceImplTest {

    @Mock
    private ReelService reelService;
    @Mock
    private RateService rateService;
    @Mock
    private BalanceService balanceService;

    @Captor
    private ArgumentCaptor<Integer> winCaptor;
    @Captor
    private ArgumentCaptor<Symbol> symbolCaptor;

    @InjectMocks
    private PlayServiceImpl playServiceImpl;

    @Test
    void play_noWin() {
        var left = List.of(Symbol.ACE, Symbol.ACE, Symbol.ACE);
        var middle = List.of(Symbol.NINE, Symbol.NINE, Symbol.NINE);
        var right = List.of(Symbol.ACE, Symbol.ACE, Symbol.ACE);
        var reels = new Reels(left, middle, right);
        when(reelService.spin()).thenReturn(reels);
        when(balanceService.add(anyInt())).thenReturn(BigDecimal.ZERO);

        var result = playServiceImpl.play(1);

        verify(balanceService).add(winCaptor.capture());
        assertThat(winCaptor.getValue()).isEqualTo(0);

        verifyNoInteractions(rateService);

        assertThat(result).isNotNull().satisfies(win -> {
            assertThat(win.getWin()).isEqualTo(0);
            assertThat(win.getBalance()).isEqualTo(BigDecimal.ZERO);
            assertThat(win.getWinningCombinations()).isEmpty();
            assertThat(win.getReels()).isSameAs(reels);
        });
    }

    @Test
    void play_win() {
        var left = List.of(Symbol.ACE, Symbol.ACE, Symbol.ACE);
        var middle = List.of(Symbol.NINE, Symbol.ACE, Symbol.NINE);
        var right = List.of(Symbol.ACE, Symbol.ACE, Symbol.ACE);
        var reels = new Reels(left, middle, right);
        when(reelService.spin()).thenReturn(reels);
        when(rateService.getBaseWin(any(Symbol.class))).thenReturn(5);
        when(balanceService.add(anyInt())).thenReturn(BigDecimal.valueOf(15));

        var result = playServiceImpl.play(1);

        verify(balanceService).add(winCaptor.capture());
        assertThat(winCaptor.getValue()).isEqualTo(15);

        verify(rateService, times(3)).getBaseWin(symbolCaptor.capture());
        assertThat(symbolCaptor.getValue()).isEqualTo(Symbol.ACE);

        assertThat(result).isNotNull().satisfies(win -> {
            assertThat(win.getWin()).isEqualTo(15);
            assertThat(win.getBalance()).isEqualTo(BigDecimal.valueOf(15));
            assertThat(win.getWinningCombinations()).allSatisfy((row, symbol) -> {
                assertThat(row).isIn(List.of(WinningRow.MIDDLE_ROW, WinningRow.DIAGONAL_FROM_LEFT_UPPER_CORNER, WinningRow.DIAGONAL_FROM_RIGHT_UPPER_CORNER));
                assertThat(symbol).isEqualTo(Symbol.ACE);
            });
            assertThat(win.getReels()).isSameAs(reels);
        });
    }
}