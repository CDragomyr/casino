package com.onetouch.casino.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class BalanceServiceImplTest {

    private final BalanceServiceImpl balanceService = new BalanceServiceImpl();

    @Test
    @Order(1)
    void add_forInitialZeroBalance() {
        var value = balanceService.add(10);
        assertThat(value).isEqualTo(BigDecimal.TEN);
    }

    @Test
    @Order(2)
    void add_forNonZeroBalance() {
        var value = balanceService.add(5);
        assertThat(value).isEqualTo(BigDecimal.valueOf(15));
    }
}