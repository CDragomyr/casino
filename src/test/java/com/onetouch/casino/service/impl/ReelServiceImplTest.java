package com.onetouch.casino.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.service.ReelService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ReelServiceImpl.class)
@TestPropertySource(locations = "classpath:test-application.properties")
class ReelServiceImplTest {

    public static final int EXPECTED_COUNT_OF_SYMBOLS_AFTER_SPIN = 3;

    @Autowired
    private ReelService reelService;

    @Test
    void loadReels() {
        assertDoesNotThrow(() -> reelService.loadReels());

        assertThat(reelService.getReels()).isNotNull().satisfies(reels -> {
            var symbols = new ArrayList<>(Arrays.asList(Symbol.values()));
            assertThat(reels.getLeft()).containsAll(symbols);
            assertThat(reels.getMiddle()).containsAll(symbols);

            // right reel does not have King symbol
            symbols.remove(Symbol.KING);
            assertThat(reels.getRight()).containsAll(symbols);
        });
    }

    @Test
    void spin() {
        assertDoesNotThrow(() -> reelService.loadReels());

        assertThat(reelService.spin()).satisfies(reels -> {
            assertThat(reels.getLeft()).hasSize(EXPECTED_COUNT_OF_SYMBOLS_AFTER_SPIN);
            assertThat(reels.getMiddle()).hasSize(EXPECTED_COUNT_OF_SYMBOLS_AFTER_SPIN);
            assertThat(reels.getRight()).hasSize(EXPECTED_COUNT_OF_SYMBOLS_AFTER_SPIN);
        });
    }
}