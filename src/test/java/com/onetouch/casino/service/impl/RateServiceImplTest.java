package com.onetouch.casino.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.service.RateService;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = RateServiceImpl.class)
@TestPropertySource(locations = "classpath:test-application.properties")
class RateServiceImplTest {

    @Autowired
    private RateService rateService;

    @Test
    void loadRates() {
        assertDoesNotThrow(() -> rateService.loadRates());

        assertThat(rateService.getRates()).allSatisfy((symbol, value) -> {
            assertThat(symbol).isIn((Object[]) Symbol.values());
            assertThat(value).isGreaterThan(0);
        });
    }
}