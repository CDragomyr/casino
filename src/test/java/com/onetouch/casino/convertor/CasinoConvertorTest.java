package com.onetouch.casino.convertor;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import com.onetouch.casino.model.Reels;
import com.onetouch.casino.model.Symbol;
import com.onetouch.casino.model.Win;
import com.onetouch.casino.model.WinningRow;

class CasinoConvertorTest {

    private static final Function<List<Symbol>, List<String>> SYMBOL_NAMES = symbols -> symbols.stream()
                                                                                               .map(Enum::name)
                                                                                               .map(String::toLowerCase)
                                                                                               .map(StringUtils::capitalize)
                                                                                               .collect(Collectors.toList());
    private static final Function<List<WinningRow>, List<String>> WINNING_ROW_DESCRIPTIONS = rows -> rows.stream()
                                                                                                         .map(WinningRow::getDescription)
                                                                                                         .collect(Collectors.toList());
    private static final Function<Symbol, String> SYMBOL_NAME = symbol -> Optional.of(symbol)
                                                                                  .map(Enum::name)
                                                                                  .map(String::toLowerCase)
                                                                                  .map(StringUtils::capitalize)
                                                                                  .get();

    @Test
    void toResponse_noWin() {
        var reels = new Reels(
                List.of(Symbol.ACE, Symbol.NINE, Symbol.EIGHT),
                List.of(Symbol.NINE, Symbol.ACE, Symbol.EIGHT),
                List.of(Symbol.ACE, Symbol.NINE, Symbol.NINE));
        var model = new Win(0, BigDecimal.ZERO, Map.of(), reels);

        var dto = CasinoConvertor.toResponse(model);
        assertThat(dto).satisfies(winResponse -> {
            assertThat(winResponse.getWin()).isEqualTo(0);
            assertThat(winResponse.getBalance()).isEqualTo(BigDecimal.ZERO);
            assertThat(winResponse.getWinningCombinations()).isEmpty();
            assertThat(winResponse.getReels()).satisfies(reelsResponse -> {
                assertThat(reelsResponse.getLeftReel()).containsAll(SYMBOL_NAMES.apply(reels.getLeft()));
                assertThat(reelsResponse.getMiddleReel()).containsAll(SYMBOL_NAMES.apply(reels.getMiddle()));
                assertThat(reelsResponse.getRightReel()).containsAll(SYMBOL_NAMES.apply(reels.getRight()));
            });
        });
    }

    @Test
    void toResponse_win() {
        var reels = new Reels(
                List.of(Symbol.ACE, Symbol.ACE, Symbol.ACE),
                List.of(Symbol.NINE, Symbol.ACE, Symbol.EIGHT),
                List.of(Symbol.ACE, Symbol.ACE, Symbol.ACE));
        var combinations = Map.of(
                WinningRow.MIDDLE_ROW, Symbol.ACE,
                WinningRow.DIAGONAL_FROM_LEFT_UPPER_CORNER, Symbol.ACE,
                WinningRow.DIAGONAL_FROM_RIGHT_UPPER_CORNER, Symbol.ACE);
        var model = new Win(10, BigDecimal.TEN, combinations, reels);

        var dto = CasinoConvertor.toResponse(model);
        assertThat(dto).satisfies(winResponse -> {
            assertThat(winResponse.getWin()).isEqualTo(10);
            assertThat(winResponse.getBalance()).isEqualTo(BigDecimal.TEN);
            assertThat(winResponse.getWinningCombinations()).allSatisfy((row, symbol) -> {
                assertThat(row).isIn(WINNING_ROW_DESCRIPTIONS.apply(List.of(WinningRow.MIDDLE_ROW, WinningRow.DIAGONAL_FROM_LEFT_UPPER_CORNER, WinningRow.DIAGONAL_FROM_RIGHT_UPPER_CORNER)));
                assertThat(symbol).isEqualTo(SYMBOL_NAME.apply(Symbol.ACE));
            });
            assertThat(winResponse.getReels()).satisfies(reelsResponse -> {
                assertThat(reelsResponse.getLeftReel()).containsAll(SYMBOL_NAMES.apply(reels.getLeft()));
                assertThat(reelsResponse.getMiddleReel()).containsAll(SYMBOL_NAMES.apply(reels.getMiddle()));
                assertThat(reelsResponse.getRightReel()).containsAll(SYMBOL_NAMES.apply(reels.getRight()));
            });
        });
    }
}